;;; teamacs.el --- Description -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2021 Arlo Hobbs
;;
;; Author: Arlo Hobbs <http://github/bond7o6>
;; Maintainer: Arlo Hobbs <bond7o6@hotmail.com>
;; Created: January 23, 2021
;; Modified: January 23, 2021
;; Version: 0.0.1
;; Keywords: Symbol’s value as variable is void: finder-known-keywords
;; Homepage: https://github.com/bond7o6/teamacs
;; Package-Requires: ((emacs "24.3"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:



;;; Prerequisites



;;; Constants



;;; Customs

(defcustom tea~draw-char-vertical-line ?|
  "The character used to draw vertical lines."
  :group 'tea
  :type 'character)

(defcustom tea~draw-char-horizontal-line ?-
  "The character used to draw horizontal lines."
  :group 'tea
  :type 'character)

(defcustom tea~draw-char-juction ?+
  "The character used to draw junctions."
  :group 'tea
  :type 'character)

(defcustom tea~draw-char-top-right-corner ?+
  "The character used to draw junctions at the top right corner."
  :group 'tea
  :type 'character)

(defcustom tea~draw-char-top-left-corner ?+
  "The character used to draw junctions at the top left corner."
  :group 'tea
  :type 'character)

(defcustom tea~draw-char-left-junction ?+
  "The character used to draw junctions at the left border."
  :group 'tea
  :type 'character)

(defcustom tea~draw-char-right-junction ?+
  "The character used to draw junctions at the right border."
  :group 'tea
  :type 'character)

(defcustom tea~draw-char-top-junction ?+
  "The character used to draw junctions at the top border."
  :group 'tea
  :type 'character)

(provide 'teamacs)
;;; teamacs.el ends here
